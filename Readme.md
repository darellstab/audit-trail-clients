# Audit Trail Prototype Clients

Steps to run the clients:
1. `npm install -g serve`
2. `npm ci`
3. `server -s build`
4. Open `http://localhost:5000/audit`

Start DEV-Server: `npm run start`

Create production build: `npm run build`
