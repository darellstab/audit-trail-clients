import React from 'react';
import './App.css';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import AuditClient from "./apps/AuditClient";
import CheckpointClient from "./apps/CheckpointClient";

export default function App() {

    return (
        <Router>
            <div>
                <nav>
                    <Link to="/audit">Audit</Link> &nbsp;
                    <Link to="/checkpoint">Checkpoint</Link>
                </nav>

                {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
                <Switch>
                    <Route path="/audit">
                        <AuditClient />
                    </Route>
                    <Route path="/checkpoint">
                        <CheckpointClient />
                    </Route>
                    <Route path="/">
                        <span>Select client</span>
                    </Route>
                </Switch>
            </div>
        </Router>
    );

}
