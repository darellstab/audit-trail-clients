
export enum MeasurementStatus {
    CHECKIN = 1,
    CHECKOUT = 2
}

export type Measurement = {
    sensorId: number,
    qualityScore: number,
    status: MeasurementStatus,
    personId: number,
    objectId: number,
    timestamp: number
}

export type GroupedMeasurement = {
    status: MeasurementStatus,
    personId: number,
    objectId: number,
    timestamp: number,
    measurements: SensorReadOut[]
}

export type SensorReadOut = {
    sensorId: number,
    qualityScore: number,
}
