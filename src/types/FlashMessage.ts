
export enum FlashMessageStatus {
    SUCCESS = 'success',
    ERROR = 'error'
}

export type FlashMessage = {
    status: FlashMessageStatus,
    message: string
}
