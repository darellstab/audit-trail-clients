import axios from 'axios';
import {Measurement, MeasurementStatus} from "../types/Measurement";

const baseUrl = 'http://127.0.0.1:1880/';
const measurementsListEndpoint = 'audit/measurments/list';
const deObfuscatePersonIdEndpoint = 'hr-system/deobfuscate/';
const checkpointEndpoint = 'audit/check-point/';

export function getMeasurements() : Promise<Measurement[]> {
    return axios.get(baseUrl + measurementsListEndpoint)
        .then(response => response.data);
}

export function getDeObfuscatedPersonId(obfuscatedPersonId : number) : Promise<number> {
    const config = {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    };

    const params = new URLSearchParams();
    params.append('obfuscatedId', obfuscatedPersonId.toString());

    return axios.post(baseUrl + deObfuscatePersonIdEndpoint, params, config)
        .then(response => response.data);
}

export function submitCheckpointData(status : MeasurementStatus, personId : number, objectId : number) : Promise<null> {
    const config = {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    };

    const params = new URLSearchParams();
    params.append('status', status.toString());
    params.append('personId', personId.toString());
    params.append('objectId', objectId.toString());

    return axios.post(baseUrl + checkpointEndpoint, params, config)
        .then(response => response.data);
}
