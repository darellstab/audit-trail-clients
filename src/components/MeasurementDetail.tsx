import React, {MouseEvent, useState} from 'react';
import {GroupedMeasurement, MeasurementStatus} from "../types/Measurement";
import {getDeObfuscatedPersonId} from "../services/api";
import {FlashMessageContainer} from "./FlashMessageContainer";
import {FlashMessage, FlashMessageStatus} from "../types/FlashMessage";
import {formatDateTime} from "../utility/timestampUtility";

type MeasurementDetailProps = {
    measurement: GroupedMeasurement,
}

export function MeasurementDetail({measurement} : MeasurementDetailProps) {

    const [showDetails, setShowDetails] = useState<boolean>(false);
    const [resolvedPersonId, setResolvedPersonId] = useState<number>(0);
    const [flashMessage, setFlashMessage] = useState<FlashMessage|null>(null);

    const deObfuscatePersonId = (event : MouseEvent) => {
        event.preventDefault();
        getDeObfuscatedPersonId(measurement.personId).then(result => {
            if(result === 0) {
                setFlashMessage({message: 'Not able to resolve the Person-ID', status: FlashMessageStatus.ERROR})
            } else {
                setResolvedPersonId(result);
            }
        });
    };

    return (
        <div className={'measurementContainer container'}>
            <span onClick={() => setShowDetails(!showDetails)}>
                {formatDateTime(measurement.timestamp)} ({measurement.status === MeasurementStatus.CHECKIN ? 'Check-In' : 'Check-Out'})
            </span>
            {showDetails &&
                <ul>
					<li>Object-ID: {measurement.objectId}</li>
                    {resolvedPersonId === 0 ?
					    <li>Person-ID (Obfuscated): {measurement.personId} <a href='' onClick={deObfuscatePersonId}>Resolve</a></li> :
                        <li>Person-ID (Resolved): {resolvedPersonId}</li>
                    }
                    {measurement.measurements.map(item => <li key={item.sensorId}>Sensor {item.sensorId}: {item.qualityScore}</li>)}

                    <FlashMessageContainer flashMessage={flashMessage} />
                </ul>
            }

        </div>
    );
}
