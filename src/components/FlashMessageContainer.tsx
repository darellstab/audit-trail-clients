import React from 'react';
import {FlashMessage} from "../types/FlashMessage";

type FlashMessageContainerProps = {
    flashMessage: FlashMessage|null,
}

export function FlashMessageContainer({flashMessage} : FlashMessageContainerProps) {

    if(flashMessage === null) {
        return null;
    }

    return (
        <div className={'container flashMessageContainer ' + flashMessage.status.toString()}>
            {flashMessage.message}
        </div>
    );
}
