import React, {useState} from 'react';
import {MeasurementStatus} from "../types/Measurement";
import {availableObjects} from "../data/objects";
import {submitCheckpointData} from "../services/api";
import {FlashMessage, FlashMessageStatus} from "../types/FlashMessage";
import {FlashMessageContainer} from "../components/FlashMessageContainer";

export default function CheckpointClient() {

    const [checkpointStatus, setCheckpointStatus] = useState<MeasurementStatus>(MeasurementStatus.CHECKIN);
    const [selectedObject, setSelectedObject] = useState<number>(0);
    const [personId, setPersonId] = useState<string>('');
    const [flashMessage, setFlashMessage] = useState<FlashMessage|null>(null);

    const submitData = () => {
        submitCheckpointData(checkpointStatus, parseInt(personId, 10), selectedObject)
            .then(response => {
                setCheckpointStatus(checkpointStatus === MeasurementStatus.CHECKIN ? MeasurementStatus.CHECKOUT : MeasurementStatus.CHECKIN)
                setFlashMessage({message: checkpointStatus === MeasurementStatus.CHECKIN ? 'Check-In successful!' : 'Check-Out successful!', status: FlashMessageStatus.SUCCESS})
            })
            .catch(error => setFlashMessage({ message: 'Checkpoint Error: ' + error.message, status: FlashMessageStatus.ERROR}))
    };

    return (
        <div className="CheckpointClientApp">
            <h1>Checkpoint Client</h1>

            <div className={'container'}>
                <span className={'container'}>
                    <select value={selectedObject} onChange={(event) => setSelectedObject(parseInt(event.target.value, 10))}>
                        <option value={0}>Objekt auswählen ...</option>
                        {availableObjects.map(item => <option key={item} value={item}>Objekt-ID {item}</option>)}
                    </select>
                </span>

                <span className={'container'}>
                    <input value={personId} onChange={(event) => setPersonId(event.target.value)} placeholder={'Person-ID'} />
                </span>
            </div>

            <div className={'container'}>
                <button className={'checkpointButton'} onClick={submitData}>
                    {checkpointStatus === MeasurementStatus.CHECKIN ? 'Check-In' : 'Check-Out'}
                </button>
            </div>

            <FlashMessageContainer flashMessage={flashMessage} />


        </div>
    );
}
