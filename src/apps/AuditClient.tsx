import React, {useEffect, useState, useMemo} from 'react';
import {Measurement} from "../types/Measurement";
import {getMeasurements} from "../services/api";
import {MeasurementDetail} from "../components/MeasurementDetail";
import {filterAndGroupList} from "../utility/filterUtility";
import {formatDate, getStartOfDay} from "../utility/timestampUtility";

export default function AuditClient() {

    const [measurements, setMeasurements] = useState<Measurement[]>([]);
    const [availableObjects, setAvailableObjects] = useState<number[]>([]);
    const [selectedObject, setSelectedObject] = useState<number>(0);
    const [availableDates, setAvailableDates] = useState<number[]>([]);
    const [selectedDate, setSelectedDate] = useState<number>(0);

    useEffect(() => {
        fetchData();
    }, []);

    useEffect(() => {
        let currentMeasurements = measurements;
        if (selectedObject !== 0) {
            currentMeasurements = measurements.filter(item => item.objectId === selectedObject);
        }
        const allDates = currentMeasurements.map(item => getStartOfDay(item.timestamp));
        setAvailableDates([...new Set(allDates)]);

    }, [measurements, selectedObject]);

    const fetchData = () => {
        getMeasurements().then(result => {
            setMeasurements(result);
            const allObjectIds = result.map(item => item.objectId);
            setAvailableObjects([...new Set(allObjectIds)]);
        });
    };

    const filteredAndGroupedList = useMemo(() => {
        return filterAndGroupList(measurements, selectedDate, selectedObject);
    }, [measurements, selectedDate, selectedObject]);

    return (
        <div className="auditClientApp">
            <h1>Audit Client</h1>

            <div className={'filterContainer container'}>
                <span className={'container'}>
                    <select value={selectedObject}
                            onChange={(event) => setSelectedObject(parseInt(event.target.value, 10))}>
                        <option value={0}>Objekt auswählen ...</option>
                        {availableObjects.map(item => <option key={item} value={item}>Objekt-ID {item}</option>)}
                    </select>
                </span>

                {selectedObject !== 0 &&
				<span className={'container'}>
                    <select value={selectedDate} onChange={event => setSelectedDate(parseInt(event.target.value, 10))}>
                        <option value={0}>Tag auswählen ...</option>
                        {availableDates.map(item => <option key={item}
                                                            value={item}>{formatDate(item)}</option>)}
                    </select>
                </span>
                }
                <button onClick={fetchData}>Refresh Data</button>
            </div>

            {selectedObject !== 0 && measurements &&
			<div className={'measurementListContainer container'}>
                {
                    filteredAndGroupedList.map((item, index) => <MeasurementDetail measurement={item} key={index}/>)
                }
			</div>
            }
        </div>
    );
}
