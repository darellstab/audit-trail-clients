import moment from "moment";


export function getStartOfDay(timestampInMs : number) : number {
    return moment.unix(timestampInMs / 1000).set({
        'hour': 0,
        'minutes': 0,
        'seconds': 0
    }).unix();
}

export function getEndOfDay(timestampInS : number) : number {
    return moment.unix(timestampInS).set({
        'hour': 23,
        'minutes': 59,
        'seconds': 59
    }).unix();
}

export function formatDate(timestampInS : number) : string {
    return moment.unix(timestampInS).format('DD.MM.YYYY');
}

export function formatDateTime(timestampInMs : number) : string {
    return moment.unix(timestampInMs / 1000).format('DD.MM.YYYY - HH:mm:ss');
}
