import {GroupedMeasurement, Measurement} from "../types/Measurement";
import {getEndOfDay} from "./timestampUtility";


export function filterAndGroupList(measurements: Measurement[], selectedDate: number, selectedObject: number) : GroupedMeasurement[] {
    const groupedMeasurements = new Map();

    measurements.forEach((item) => {
        if(groupedMeasurements.has(item.timestamp)) {
            const measurementToAppend = groupedMeasurements.get(item.timestamp);
            measurementToAppend.measurements.push({sensorId: item.sensorId, qualityScore: item.qualityScore})
            groupedMeasurements.set(item.timestamp, measurementToAppend);
        } else {
            groupedMeasurements.set(item.timestamp, {
                status: item.status,
                objectId: item.objectId,
                personId: item.personId,
                timestamp: item.timestamp,
                measurements: [{sensorId: item.sensorId, qualityScore: item.qualityScore}]
            })
        }
    });

    return Array.from(groupedMeasurements.values())
        .filter(item => {
            const itemTimestamp = item.timestamp / 1000;
            const endOfDay = getEndOfDay(selectedDate);

            return item.objectId === selectedObject &&
                (selectedDate === 0 || (
                    itemTimestamp > selectedDate &&
                    itemTimestamp < endOfDay
                ));
        })
}
